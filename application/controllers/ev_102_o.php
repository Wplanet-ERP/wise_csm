<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ev_102_o extends CI_Controller
{
    protected $_url = "ev_102_o";
    protected $_finish_url = "/finish";
    protected $_staff_no = "102";
    protected $_type = "1";

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('csm_save');
        $this->load-> helper(array('url', 'date', 'alert'));
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $check_data = array(
            "s_no"      => $this->_staff_no,
            "type"      => $this->_type,
            "ip"        => $_SERVER['REMOTE_ADDR']
        );

        $result = $this->csm_save->check_ip($check_data);

        if($result) {
            alert("이미 참여하셨습니다.", 'finish');
        }else{
            $this->load->view('evaluation_temp');
        }
	}

	public function save()
    {
        if ($_POST)
        {
            $insert_data = array(
                "s_no"      => $this->_staff_no,
                "type"      => $this->_type,
                "ip"        => $_SERVER['REMOTE_ADDR'],
                "q1_score"  => $this->input->post('q1'),
                "q2_score"  => $this->input->post('q2'),
                "q3_score"  => $this->input->post('q3'),
                "q4_score"  => $this->input->post('q4'),
                "q5_text"   => trim(addslashes($this->input->post('q5')))
            );

            $result = $this->csm_save->save($insert_data);

            if($result){
                alert("참여해주셔서 감사합니다.", $this->_finish_url);
            }else{
                alert("오류가 발생했습니다. 다시 이용해주세요", $this->_url);
            }
        }
    }
}

/* End of file index.php */
/* Location: ./application/controllers/index.php */