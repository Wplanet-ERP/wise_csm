<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 저장
 */
class Csm_save extends CI_Model
{
    protected $_table_name = "csm_evaluation";

    function __construct() {
        parent::__construct();
    }

    function check_ip($check_data)
    {
        $result = false;

        $check_sql    = "SELECT * FROM csm_evaluation WHERE ip='{$check_data['ip']}' AND `type`='{$check_data['type']}' AND s_no='{$check_data['s_no']}'";
        $check_query  = $this->db->query($check_sql);
        $check_result = $check_query->num_rows();

        if($check_result){
            $result = $check_result > 0 ? true : false;
        }

        return $result;
    }

    function save($data)
    {
        date_Default_TimeZone_set("Asia/Seoul");

        $insert_data = array(
            "s_no"      => $data['s_no'],
            "type"      => $data['type'],
            "ip"        => $data['ip'],
            "q1_score"  => $data['q1_score'],
            "q2_score"  => $data['q2_score'],
            "q3_score"  => $data['q3_score'],
            "q4_score"  => $data['q4_score'],
            "q5_text"   => $data['q5_text'],
            'regdate'   => date("Y-m-d H:i:s")
        );

        $result = $this->db->insert($this->_table_name, $insert_data);

        return $result;
    }
}