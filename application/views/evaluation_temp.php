<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Belabef 고객만족도 조사</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta name="robots" content="noindex, nofollow" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-1.9.1.min.js" charset="utf-8"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="/js/main.js" charset="utf-8"></script>
    <link type="text/css" rel="stylesheet" href="/css/main.css" />
</head>
<body>

<div id="csm-container">
    <div id="header-content">
        <h2>베라베프 고객만족도 평가</h2>
        <p>
            고객님 베라베프 직원과의 상담은 어떠셨나요?<br/>
            고객님의 소중한 의견을 바탕으로 더욱 만족드리는 서비스를 제공 하도록 하겠습니다.<br/>
            바쁘시겠지만 상담 만족도 조사에 참여 부탁드립니다.<br/>
        </p>
    </div>
	<div id="csm-content">
        <form id="csm_frm" name="csm_frm" action="" method="post">
            <div class="form-group">
                <label class="col-form-label font-weight-bold" >1.고객센터의 온라인상담과 ARS 상담 서비스에 만족 하셨나요?</label>
                <div class="form-group">
                    <div class="form-check form-check-inline" style="padding-top: 6px;">
                        <input type="radio" name="q1" value="1" id="q1_result_1" class="form-check-input" required>
                        <label for="q1_result_1" class="form-check-label">1점</label>
                        <input type="radio" name="q1" value="2" id="q1_result_2" class="form-check-input" required>
                        <label for="q1_result_2" class="form-check-label">2점</label>
                        <input type="radio" name="q1" value="3" id="q1_result_3" class="form-check-input" required>
                        <label for="q1_result_3" class="form-check-label">3점</label>
                        <input type="radio" name="q1" value="4" id="q1_result_4" class="form-check-input" required>
                        <label for="q1_result_4" class="form-check-label">4점</label>
                        <input type="radio" name="q1" value="5" id="q1_result_5" class="form-check-input" required>
                        <label for="q1_result_5" class="form-check-label">5점</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-form-label font-weight-bold" >2.상담시 안내 받으신 정보가 원하시는 정확한 안내가 되셨나요?</label>
                <div class="form-group">
                    <div class="form-check form-check-inline" style="padding-top: 6px;">
                        <input type="radio" name="q2" value="1" id="q2_result_1" class="form-check-input" required>
                        <label for="q2_result_1" class="form-check-label">1점</label>
                        <input type="radio" name="q2" value="2" id="q2_result_2" class="form-check-input" required>
                        <label for="q2_result_2" class="form-check-label">2점</label>
                        <input type="radio" name="q2" value="3" id="q2_result_3" class="form-check-input" required>
                        <label for="q2_result_3" class="form-check-label">3점</label>
                        <input type="radio" name="q2" value="4" id="q2_result_4" class="form-check-input" required>
                        <label for="q2_result_4" class="form-check-label">4점</label>
                        <input type="radio" name="q2" value="5" id="q2_result_5" class="form-check-input" required>
                        <label for="q2_result_5" class="form-check-label">5점</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-form-label font-weight-bold" >3.베라베프 상담채널을 다른 사람에게 추천 할 의향이 있으신가요?</label>
                <div class="form-group">
                    <div class="form-check form-check-inline" style="padding-top: 6px;">
                        <input type="radio" name="q3" value="1" id="q3_result_1" class="form-check-input" required>
                        <label for="q3_result_1" class="form-check-label">1점</label>
                        <input type="radio" name="q3" value="2" id="q3_result_2" class="form-check-input" required>
                        <label for="q3_result_2" class="form-check-label">2점</label>
                        <input type="radio" name="q3" value="3" id="q3_result_3" class="form-check-input" required>
                        <label for="q3_result_3" class="form-check-label">3점</label>
                        <input type="radio" name="q3" value="4" id="q3_result_4" class="form-check-input" required>
                        <label for="q3_result_4" class="form-check-label">4점</label>
                        <input type="radio" name="q3" value="5" id="q3_result_5" class="form-check-input" required>
                        <label for="q3_result_5" class="form-check-label">5점</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-form-label font-weight-bold" >4.상담 요청 후 상담사와 연결까지 소요시간이 적절하였습니까?</label>
                <div class="form-group">
                    <div class="form-check form-check-inline" style="padding-top: 6px;">
                        <input type="radio" name="q4" value="1" id="q4_result_1" class="form-check-input" required>
                        <label for="q4_result_1" class="form-check-label">1점</label>
                        <input type="radio" name="q4" value="2" id="q4_result_2" class="form-check-input" required>
                        <label for="q4_result_2" class="form-check-label">2점</label>
                        <input type="radio" name="q4" value="3" id="q4_result_3" class="form-check-input" required>
                        <label for="q4_result_3" class="form-check-label">3점</label>
                        <input type="radio" name="q4" value="4" id="q4_result_4" class="form-check-input" required>
                        <label for="q4_result_4" class="form-check-label">4점</label>
                        <input type="radio" name="q4" value="5" id="q4_result_5" class="form-check-input" required>
                        <label for="q4_result_5" class="form-check-label">5점</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-form-label font-weight-bold" >5.베라베프 고객센터를 이용하면서 좋았던 점이나 바라는 점이 있다면 무엇인가요?</label>
                <div class="form-group">
                    <textarea class="form-control" id="q5" name="q5" rows="5"></textarea>
                </div>
            </div>
            <div class="form-button">
                <button type="submit" class="btn btn-success font-weight-bold" onclick="return submitCheckAction(this.form);"><span>작성완료</span></button>
            </div>
        </form>
    </div>

  <p class="footer">
        (주)와이즈미디어커머스&nbsp;&nbsp;|&nbsp;&nbsp;대표 : 주경민<br/>
		     사업자등록번호 : 440-88-00977<br/>
        주소 : 서울시 금천구 벚꽃로 244 벽산디지털밸리5차 902호<br/>
        고객센터 : 1668-3620<br/>
        Copyright ⓒ 2021 베라베프(belabef) All rights reserved<br/>
  </p>
</div>
</body>
</html>
