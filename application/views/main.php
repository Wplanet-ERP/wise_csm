<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>베라베프 고객만족도</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-1.9.1.min.js" charset="utf-8"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link type="text/css" rel="stylesheet" href="/css/main.css" />
</head>
<body>

<div id="container">
	<div class="header-logo">
        <img src="/images/belabef.png" />
    </div>

	<div id="content">
        고객님의 소중한 의견은<br/>
        서비스 개선 목적에 한해 활용 됩니다.<br/><br/>
        더욱 가치 있는 서비스를 전달해드리는<br/>
        베라베프가 되도록 노력하겠습니다.<br/><br/>
        감사합니다.<br/>
    </div>

	<p class="footer">
    		(주)와이즈미디어커머스&nbsp;&nbsp;|&nbsp;&nbsp;대표 : 주경민<br/>
				사업자등록번호 : 440-88-00977<br/>
    		주소 : 서울시 금천구 벚꽃로 244 벽산디지털밸리5차 902호<br/>
				고객센터 : 1668-3620<br/>
    		Copyright ⓒ 2021 베라베프(belabef) All rights reserved<br/>
  </p>
</div>

</body>
</html>
